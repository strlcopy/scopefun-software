#include <scopefun/gui/fbOsciloskopHardwareGenerator.h>

OsciloskopHardwareGenerator::OsciloskopHardwareGenerator(wxWindow* parent)
    :
    HardwareGenerator(parent)
{
}

void OsciloskopHardwareGenerator::HardwareGeneratorOnInitDialog(wxInitDialogEvent& event)
{
    // TODO: Implement HardwareGeneratorOnInitDialog
}

void OsciloskopHardwareGenerator::m_comboBoxTypeOnCombobox(wxCommandEvent& event)
{
    // TODO: Implement m_comboBoxTypeOnCombobox
}

void OsciloskopHardwareGenerator::m_textCtrlFrequencyOnTextEnter(wxCommandEvent& event)
{
    // TODO: Implement m_textCtrlFrequencyOnTextEnter
}

void OsciloskopHardwareGenerator::m_textCtrlVoltage0OnTextEnter(wxCommandEvent& event)
{
    // TODO: Implement m_textCtrlVoltage0OnTextEnter
}

void OsciloskopHardwareGenerator::m_spinBtnCh0VoltOnSpinDown(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnCh0VoltOnSpinDown
}

void OsciloskopHardwareGenerator::m_spinBtnCh0VoltOnSpinUp(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnCh0VoltOnSpinUp
}

void OsciloskopHardwareGenerator::m_sliderVoltageOnScroll(wxScrollEvent& event)
{
    // TODO: Implement m_sliderVoltageOnScroll
}

void OsciloskopHardwareGenerator::m_textCtrlOffset0OnTextEnter(wxCommandEvent& event)
{
    // TODO: Implement m_textCtrlOffset0OnTextEnter
}

void OsciloskopHardwareGenerator::m_spinBtnCh0OffsetOnSpinDown(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnCh0OffsetOnSpinDown
}

void OsciloskopHardwareGenerator::m_spinBtnCh0OffsetOnSpinUp(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnCh0OffsetOnSpinUp
}

void OsciloskopHardwareGenerator::m_sliderOffsetOnScroll(wxScrollEvent& event)
{
    // TODO: Implement m_sliderOffsetOnScroll
}

void OsciloskopHardwareGenerator::m_textCtrlSquareDuty0OnTextEnter(wxCommandEvent& event)
{
    // TODO: Implement m_textCtrlSquareDuty0OnTextEnter
}

void OsciloskopHardwareGenerator::m_spinBtnGen0SqrDutyOnSpinDown(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnGen0SqrDutyOnSpinDown
}

void OsciloskopHardwareGenerator::m_spinBtnGen0SqrDutyOnSpinUp(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnGen0SqrDutyOnSpinUp
}

void OsciloskopHardwareGenerator::m_sliderSquareDutyOnScroll(wxScrollEvent& event)
{
    // TODO: Implement m_sliderSquareDutyOnScroll
}

void OsciloskopHardwareGenerator::m_filePicker1OnFileChanged(wxFileDirPickerEvent& event)
{
    // TODO: Implement m_filePicker1OnFileChanged
}

void OsciloskopHardwareGenerator::m_buttonCustomFileOnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonCustomFileOnButtonClick
}

void OsciloskopHardwareGenerator::m_buttonOn0OnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonOn0OnButtonClick
}

void OsciloskopHardwareGenerator::m_buttonOff0OnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonOff0OnButtonClick
}

void OsciloskopHardwareGenerator::m_comboBoxType1OnChoice(wxCommandEvent& event)
{
    // TODO: Implement m_comboBoxType1OnChoice
}

void OsciloskopHardwareGenerator::m_textCtrlFrequency1OnTextEnter(wxCommandEvent& event)
{
    // TODO: Implement m_textCtrlFrequency1OnTextEnter
}

void OsciloskopHardwareGenerator::m_textCtrlVoltage1OnTextEnter(wxCommandEvent& event)
{
    // TODO: Implement m_textCtrlVoltage1OnTextEnter
}

void OsciloskopHardwareGenerator::m_spinBtnCh1VoltOnSpinDown(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnCh1VoltOnSpinDown
}

void OsciloskopHardwareGenerator::m_spinBtnCh1VoltOnSpinUp(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnCh1VoltOnSpinUp
}

void OsciloskopHardwareGenerator::m_sliderVoltage1OnScroll(wxScrollEvent& event)
{
    // TODO: Implement m_sliderVoltage1OnScroll
}

void OsciloskopHardwareGenerator::m_textCtrlOffset1OnTextEnter(wxCommandEvent& event)
{
    // TODO: Implement m_textCtrlOffset1OnTextEnter
}

void OsciloskopHardwareGenerator::m_spinBtnCh1OffsetOnSpinDown(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnCh1OffsetOnSpinDown
}

void OsciloskopHardwareGenerator::m_spinBtnCh1OffsetOnSpinUp(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnCh1OffsetOnSpinUp
}

void OsciloskopHardwareGenerator::m_sliderOffset1OnScroll(wxScrollEvent& event)
{
    // TODO: Implement m_sliderOffset1OnScroll
}

void OsciloskopHardwareGenerator::m_textCtrlSquareDuty1OnTextEnter(wxCommandEvent& event)
{
    // TODO: Implement m_textCtrlSquareDuty1OnTextEnter
}

void OsciloskopHardwareGenerator::m_spinBtnGen1SqrDutyOnSpinDown(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnGen1SqrDutyOnSpinDown
}

void OsciloskopHardwareGenerator::m_spinBtnGen1SqrDutyOnSpinUp(wxSpinEvent& event)
{
    // TODO: Implement m_spinBtnGen1SqrDutyOnSpinUp
}

void OsciloskopHardwareGenerator::m_sliderSquareDuty1OnScroll(wxScrollEvent& event)
{
    // TODO: Implement m_sliderSquareDuty1OnScroll
}

void OsciloskopHardwareGenerator::m_filePicker11OnFileChanged(wxFileDirPickerEvent& event)
{
    // TODO: Implement m_filePicker11OnFileChanged
}

void OsciloskopHardwareGenerator::m_buttonCustomFile1OnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonCustomFile1OnButtonClick
}

void OsciloskopHardwareGenerator::m_buttonOn1OnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonOn1OnButtonClick
}

void OsciloskopHardwareGenerator::m_buttonOff1OnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonOff1OnButtonClick
}

void OsciloskopHardwareGenerator::m_buttonOkOnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonOkOnButtonClick
}

void OsciloskopHardwareGenerator::m_buttonDefaultOnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonDefaultOnButtonClick
}

void OsciloskopHardwareGenerator::m_buttonCancelOnButtonClick(wxCommandEvent& event)
{
    // TODO: Implement m_buttonCancelOnButtonClick
}
