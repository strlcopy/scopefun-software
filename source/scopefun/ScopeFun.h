////////////////////////////////////////////////////////////////////////////////
//    ScopeFun Oscilloscope ( http://www.scopefun.com )
//    Copyright (C) 2016 - 2021 David Košenina
//    Copyright (C) 2021 - 2022 Dejan Priveršek
//
//    This file is part of ScopeFun Oscilloscope.
//
//    ScopeFun Oscilloscope is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ScopeFun Oscilloscope is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this ScopeFun Oscilloscope.  If not, see <http://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __OSCILOSCOPE__
#define __OSCILOSCOPE__

// engine
#include <core/core.h>

// api
extern "C" {
#include <api/scopefunapi.h>
}

// osciloscope
#include <scopefun/window/tool.h>
#include <scopefun/osc/oscsignal.h>
#include <scopefun/osc/oscfile.h>
#include <scopefun/osc/oscsettings.h>
#include <scopefun/osc/oscfft.h>
#include <scopefun/osc/oscrender.h>
#include <scopefun/window/wnddisplay.h>
#include <scopefun/window/wndgenerate.h>
#include <scopefun/window/wndshadow.h>
#include <scopefun/window/wndmain.h>
#include <scopefun/osc/osccontrol.h>
#include <scopefun/osc/oscmng.h>

#endif
////////////////////////////////////////////////////////////////////////////////
//
//
//
////////////////////////////////////////////////////////////////////////////////
